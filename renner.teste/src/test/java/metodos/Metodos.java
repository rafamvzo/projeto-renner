package metodos;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class Metodos extends Browser {
	
	
	//Metodos CLICAR 
	public void clik(By elemento) throws InterruptedException {
		driver.findElement(elemento).click();
	}
	
	
	//Metodo ESCREVER
	public void escrever(By elemento, String texto) {
		driver.findElement(elemento).sendKeys(texto);
	}
	
	
	//Metodo SCREEN SHOT
	public void screenshot(String nome) throws IOException {
		String validaNome = nome;
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("./src/evidencia/" + validaNome + ".png"));
	}
	
	//Metodo AGUARDAR
	public void aguardar() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 
	}

	//Metodo FECHAR NAVEGADOR
	public void fecharNavegador() {
		driver.quit();
		
	}
	
	//Metedo VALIDA TEXTO
	public void validaTexto(String texto, By elemento) {
		String textoEsperado = texto;
		String validandoElemento = driver.findElement(elemento).getText();
		Assert.assertEquals(textoEsperado, validandoElemento);	
	}
	
	//Metodo MOVER MOUSER
	public void moverMouse(By elemento) {
	WebElement moveToElement = driver.findElement(elemento);
    Actions actionProvider = new Actions(driver);
    actionProvider.moveToElement(moveToElement).build().perform();
	}
	
	//Metodo Seleciona Tamanho M
	public void selecionaTamanho() {
		WebElement element = driver.findElement(By.xpath("//select[@class='form-control attribute_select no-print']"));
        Select sel = new Select (element);
        sel.selectByVisibleText("M");
}
}
