package testes;

import elementos.ElementosLogin;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import metodos.Browser;
import metodos.Metodos;

public class Teste extends Browser{
	
	Metodos mtd = new Metodos();
	ElementosLogin el = new ElementosLogin();
	
	//Realizar login de usuário cadastrado com sucesso
	
	@Given("estiver logado na pagina")
	public void estiver_logado_na_pagina() throws InterruptedException {
		mtd.abrirNavegado("http://automationpractice.com/index.php");
		mtd.clik(el.getBtnLogin());
		mtd.aguardar();
	    mtd.escrever(el.getInputEmailLogin(), "testedbc@rennerteste.com.br");
	    mtd.escrever(el.getInputPassLogin(), "Teste12345");
	    mtd.clik(el.getBtnSubmitLogin());
	}
	
	@When("Escolher um produto Printed Chiffon Dress")
	public void escolher_um_produto_printed_chiffon_dress() throws InterruptedException {
		mtd.clik(el.getMenuDresses());
		mtd.clik(el.getBtnSummerDres());
		mtd.clik(el.getBtnQuickView());
		mtd.aguardar();
		mtd.selecionaTamanho();
		mtd.clik(el.getColorGreen());
		mtd.clik(el.getBtnAddToCart());
		mtd.clik(el.getBtnContinueShopping());
		
	}

	@When("Escolher um produto Short Sleeve TShirts")
	public void escolher_um_produto_short_sleeve_t_shirts() throws InterruptedException {
		mtd.clik(el.getMenuTshirt());
		mtd.clik(el.getProductFadedShortSleev());
		mtd.clik(el.getColorBlue());
		mtd.clik(el.getBtnAddToCart());
		
	}
	
	@When("Escolher um produto na categoria Blouses")
	public void escolher_um_produto_na_categoria_blouses() throws InterruptedException {
		mtd.clik(el.getMenuWomen());
		mtd.clik(el.getCatMais());
		mtd.clik(el.getCatBlouses());
		mtd.clik(el.getProdBlouseImg());
		mtd.clik(el.getBtnQuantPlus());
		mtd.clik(el.getBtnAddToCart());
		mtd.clik(el.getBtnContinueShopping());
		
	}

	@When("Escolher um produto na categoria Dresses")
	public void escolher_um_produto_na_categoria_dresses() throws InterruptedException {
		mtd.clik(el.getMenuDresses());
		mtd.clik(el.getBtnSummerDres());
		mtd.clik(el.getProdDressImg());
		mtd.clik(el.getBtnAddToCart());
		mtd.clik(el.getGoToCheckout());

	}


	@Then("Finalizo a compra com sucesso")
	public void finalizo_a_compra_com_sucesso() throws InterruptedException {
		mtd.aguardar();
		mtd.clik(el.getProceedToCheckout());
		mtd.clik(el.getProceedToCheckout());
		mtd.clik(el.getCheckTrans());
		mtd.clik(el.getProceedToCheckout());
		mtd.clik(el.getBankWire());
		mtd.clik(el.getProceedToCheckout());
		mtd.validaTexto("Your order on My Store is complete.", el.getMsgValidaCompra());

	}

}
