package testes;

import elementos.ElementosCreateAccount;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import metodos.Metodos;

public class CadastrarNovaConta {
	
	Metodos mtd = new Metodos();
	ElementosCreateAccount el = new ElementosCreateAccount();
	
	@Given("Eu acessar a pagina de cadastro")
	public void eu_acessar_a_pagina_de_cadastro() throws InterruptedException {
		
		mtd.abrirNavegado("http://automationpractice.com/index.php");
		mtd.clik(el.getBtnLogin());
		mtd.aguardar();
	}
	

	@When("Preencher os dados obrigatorios")
	public void preencher_os_dados_obrigatorios() throws InterruptedException {
		
		mtd.escrever(el.getCreatInputEmail(), "testedbc@rennerteste.com.br");
		mtd.clik(el.getBtnSubmitAccount());
		
		mtd.aguardar();
		
		mtd.clik(el.getCheckGender());
		mtd.escrever(el.getFirstName(), "Santos");
		mtd.escrever(el.getLastName(), "Henrique Drumond");
		mtd.escrever(el.getPassword(), "Teste12345");
		
		mtd.escrever(el.getAdrFirstName(), "Osvaldo");
		mtd.escrever(el.getAdrLastName(), "Drumond");
		mtd.escrever(el.getAdressLineOne(), "Bonita Ave, 152");
		mtd.escrever(el.getCity(), "Long Beach");
		mtd.aguardar();
		mtd.clik(el.getState());
		mtd.aguardar();
		mtd.escrever(el.getZipCode(), "90802");
		mtd.escrever(el.getMobilePhone(), "1987654321");
		mtd.aguardar();
		mtd.clik(el.getSubmitRegister());
	}

	@Then("Validar que o Cadastro foi Efetuado com Sucesso")
	public void validar_que_o_cadastro_foi_efetuado_com_sucesso() {
		
		mtd.validaTexto("MY ACCOUNT", el.getTextMyAccount());
		mtd.fecharNavegador();

	}


}
