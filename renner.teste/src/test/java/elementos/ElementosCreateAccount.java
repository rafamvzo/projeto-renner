package elementos;

import org.openqa.selenium.By;

public class ElementosCreateAccount {
	
	//Elementos Informações Pessoais
	private By btnLogin = By.xpath("//a[@class='login']");
	private By creatInputEmail = By.id("email_create");
	private By btnSubmitAccount = By.id("SubmitCreate");
	private By checkGender = By.id("id_gender1");
	private By firstName = By.id("customer_firstname");
	private By lastName = By.id("customer_lastname");
	private By password = By.id("passwd");

	//Elementos para preencher Adress
	private By adrFirstName = By.id("passwd");
	private By adrLastName = By.id("lastname");
	private By adressLineOne = By.id("address1");
	private By city = By.id("city");
	private By state = By.xpath("//*[@id=\"id_state\"]/option[6]");
	private By zipCode = By.id("postcode");
	private By mobilePhone = By.id("phone_mobile");
	private By submitRegister = By.id("submitAccount");
	
	//Elemento para Validação de Nova Conta
	private By textMyAccount = By.xpath("//h1[@class='page-heading']");
	
	public By getBtnLogin() {
		return btnLogin;
	}
	public By getCreatInputEmail() {
		return creatInputEmail;
	}
	public By getBtnSubmitAccount() {
		return btnSubmitAccount;
	}
	public By getCheckGender() {
		return checkGender;
	}
	public By getFirstName() {
		return firstName;
	}
	public By getLastName() {
		return lastName;
	}
	public By getPassword() {
		return password;
	}
	public By getAdrFirstName() {
		return adrFirstName;
	}
	public By getAdrLastName() {
		return adrLastName;
	}
	public By getAdressLineOne() {
		return adressLineOne;
	}
	public By getCity() {
		return city;
	}
	public By getState() {
		return state;
	}
	public By getMobilePhone() {
		return mobilePhone;
	}
	public By getSubmitRegister() {
		return submitRegister;
	}
	public By getZipCode() {
		return zipCode;
	}
	public By getTextMyAccount() {
		return textMyAccount;
	}

	
	
	

}
