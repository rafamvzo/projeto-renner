package elementos;

import org.openqa.selenium.By;

public class ElementosLogin {
	
	//Elementos para o Login de um usuário cadastrado
	private By btnLogin = By.xpath("//a[@class='login']");
	private By inputEmailLogin = By.id("email");
	private By inputPassLogin = By.id("passwd");
	private By btnSubmitLogin = By.id("SubmitLogin");
	
	//Elementos compra Printed Chiffon Dress
	private By menuWomen = By.xpath("//a[@href='http://automationpractice.com/index.php?id_category=3&controller=category'][@class='sf-with-ul']");
	private By btnPrintedChiffonQv = By.xpath("//a[@rel='http://automationpractice.com/index.php?id_product=7&controller=product']");
	private By moveChiffonDress = By.xpath("//img[@src='http://automationpractice.com/img/p/2/0/20-home_default.jpg']");
	private By selectSizeChiffon = By.id("uniform-group_1");
	private By sizeMChiffon = By.xpath("//*[@id=\"group_1\"]/option[2]");
	private By colorGreen = By.id("color_15");
	private By btnQuickView = By.xpath("//*[@id=\"center_column\"]/ul/li[3]/div/div[2]/h5/a");
	
	//Elementos compra T-Shirt
	private By menuTshirt = By.xpath("//*[@id=\"block_top_menu\"]/ul/li[3]/a");
	private By productFadedShortSleev = By.xpath("//*[@id=\"center_column\"]/ul/li/div/div[2]/h5/a");
	private By colorBlue = By.id("color_14");
	
	//Elementos compra Blouses
	private By catMais = By.cssSelector("#categories_block_left > div > ul > li:nth-child(1) > span");
	private By catBlouses = By.cssSelector("#categories_block_left > div > ul > li:nth-child(1) > ul > li.last > a");
	private By prodBlouseImg = By.xpath("//*[@id=\"center_column\"]/ul/li/div/div[2]/h5/a");
	private By btnQuantPlus = By.xpath("//*[@id=\"quantity_wanted_p\"]/a[2]/span/i");
	
	//Elementos compra Dresses
	private By menuDresses = By.cssSelector("#block_top_menu > ul > li:nth-child(2) > a");
	private By btnSummerDres = By.cssSelector("#subcategories > ul > li:nth-child(3) > div.subcategory-image > a");
	private By prodDressImg = By.xpath("//*[@id=\"center_column\"]/ul/li[1]/div/div[2]/h5/a");

	//Elementos Gerais
	private By bankWire = By.xpath("//a[@class='bankwire']");
	private By btnContinueShopping = By.xpath("//span[@class='continue btn btn-default button exclusive-medium']");
	private By btnAddToCart = By.id("add_to_cart");
	private By goToCheckout = By.xpath("//a[@class='btn btn-default button button-medium']");
	private By proceedToCheckout = By.xpath("//a[@class='button btn btn-default standard-checkout button-medium']");
	private By checkTrans = By.id("uniform-cgv");
	private By msgValidaCompra = By.xpath("//p[@class='cheque-indent']");
	
	
	//Getters
	public By getBtnContinueShopping() {
		return btnContinueShopping;
	}
	public By getMenuDresses() {
		return menuDresses;
	}
	public By getBtnSummerDres() {
		return btnSummerDres;
	}
	public By getProdDressImg() {
		return prodDressImg;
	}
	public By getGoToCheckout() {
		return goToCheckout;
	}
	public By getCatMais() {
		return catMais;
	}
	public By getCatBlouses() {
		return catBlouses;
	}
	public By getProdBlouseImg() {
		return prodBlouseImg;
	}
	public By getBtnLogin() {
		return btnLogin;
	}
	public By getInputEmailLogin() {
		return inputEmailLogin;
	}
	public By getInputPassLogin() {
		return inputPassLogin;
	}
	public By getBtnSubmitLogin() {
		return btnSubmitLogin;
	}
	public By getMenuWomen() {
		return menuWomen;
	}
	public By getCheckTrans() {
		return checkTrans;
	}
	public By getBankWire() {
		return bankWire;
	}
	public By getMsgValidaCompra() {
		return msgValidaCompra;
	}
	public By getBtnQuickView() {
		return btnQuickView;
	}
	public By getBtnQuantPlus() {
		return btnQuantPlus;
	}
	public By getBtnAddToCart() {
		return btnAddToCart;
	}
	public By getBtnPrintedChiffonQv() {
		return btnPrintedChiffonQv;
	}
	public By getMoveChiffonDress() {
		return moveChiffonDress;
	}
	public By getSelectSizeChiffon() {
		return selectSizeChiffon;
	}
	public By getSizeMChiffon() {
		return sizeMChiffon;
	}
	public By getColorGreen() {
		return colorGreen;
	}
	public By getMenuTshirt() {
		return menuTshirt;
	}
	public By getProductFadedShortSleev() {
		return productFadedShortSleev;
	}
	public By getColorBlue() {
		return colorBlue;
	}
	public By getProceedToCheckout() {
		return proceedToCheckout;
	}

}
