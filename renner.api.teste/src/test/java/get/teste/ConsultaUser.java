package get.teste;




import org.hamcrest.Matchers;

import org.junit.Test;

import io.restassured.RestAssured;


public class ConsultaUser {
	
	@Test
	public void consultaUsuario() {
		
		String id = "811";
		
			
		RestAssured.given()
		.contentType("application/json")
		.when().get("https://reqres.in/api/users/"+id)
		.then().log().all()
		.statusCode(200)
		.body("first_name", Matchers.is(id));
		
		System.out.println("---Consulta realizada com sucesso---");
	}
	
	@Test
	public void consultaTodosUsuarios() {
		RestAssured.given()
		.contentType("application/json")
		.when().get("https://reqres.in/api/users?page=2")
		.then().log().all()
		.statusCode(200);
		
		System.out.println("---Consulta de todos os usu�rios realizado com sucesso---");
	}
	
	

}
